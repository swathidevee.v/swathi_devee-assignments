package org.test;

public class Child5 extends Parent5 {
	public void property3() {
		System.out.println("Child5 property");
	}
	public static void main(String[] args) {
		Child5 c=new Child5();
		Parent5 p=new Parent5();
		Grandparent5 g=new Grandparent5();
		c.property1();
		c.property2();
		c.property3();
		p.property1();
		p.property2();
		g.property1();
	}
}

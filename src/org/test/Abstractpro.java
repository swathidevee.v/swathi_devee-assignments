package org.test;

public abstract class Abstractpro {
	// Abstract class has both abstract & non abstract method
	//Abstract - No object , cannot be instantiated 
	// Child class can implement all the abstract method present in the parent
	//What happen if the child does not implement abstract method
public abstract void empId();
//Non abstract is not compulsory
public void empName() {
	
	
}
}

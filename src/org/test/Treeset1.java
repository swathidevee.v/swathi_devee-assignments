package org.test;

import java.util.TreeSet;

public class Treeset1 {
public static void main(String[] args) {
	
      TreeSet<String> ts = new TreeSet<>();

      ts.add("Welcome");
      ts.add("to");
      ts.add("Globallogic");

      System.out.println("Tree Set is " + ts);

      String check = "Globallogic";

      System.out.println("Contains " + check + " "
                         + ts.contains(check));

      
      System.out.println("First Value " + ts.first());

     
      System.out.println("Last Value " + ts.last());

}
}

package org.str;

import java.util.Scanner;

public class Palindrome1 {
public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter a string");
	String str1 = sc.nextLine();
	StringBuffer sb=new StringBuffer(str1);
	sb.reverse();
	String str2=sb.toString();
	if (str1.equals(str2)) {
		System.out.println("It is palindrome");
	}
	else {
		System.out.println("It is not palindrome");
	}
}
}
